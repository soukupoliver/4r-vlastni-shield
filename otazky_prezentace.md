# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala | - Trochu více než jsem čekal, potkali mě nějaké potíže - 40h
| jak se mi to podařilo rozplánovat | - Mohl jsem něcoudělat lépe neříkam že ne, ale trochu mě v tom nechal laskakit :/
| design zapojení | - jsem se snažil pojmout co nejvice konzervativne a aby dával smysl
| proč jsem zvolil tento design | - kvůlu sedláckému rozumu:), zdálo se mi to tak prostě přirozené
| zapojení | - https://www.youtube.com/watch?v=dWbPl8GNeOg
| z jakých součástí se zapojení skládá | LED, LED páska, Teplotní čidlo (DS18B20), Fotorezistor, LCD displej, prototype shield
| realizace | - proběhla formou pájení součástek na desku a také s použitím tavné pistole
| nápad, v jakém produktu vše propojit dohromady| - jako edukakční deska pro školy, lze s ní pak pracovat s vetší variací vlastností
| co se mi povedlo | - nakoupit vše správně (skoro), nespálit si všechny prsty (jen 4 - úspěch)
| co se mi nepovedlo/příště bych udělal/a jinak | - Výběr desky - příště více zapracovat na tom co kupuji, nakupovat jinde než na laskakit - doporučuji kamenné prodejny s elektrosoučástkami v Plzni
| zhodnocení celé tvorby | - 7/10
